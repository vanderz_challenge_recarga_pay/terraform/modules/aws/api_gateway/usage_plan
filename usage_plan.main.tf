resource "aws_api_gateway_usage_plan" "main" {
  name        = var.usage_plan_name
  description = var.usage_plan_description
  product_code = var.usage_plan_product_code

  dynamic "api_stages" {
    for_each = var.api_stages
    content {
      api_id = api_stages.value.api_id
      stage = api_stages.value.stage
    }
  }

  dynamic "quota_settings" {
    for_each = var.quota_settings
    content {
      limit = quota_settings.value.limit
      offset = quota_settings.value.offset
      period = quota_settings.value.period
    }
  }

  dynamic "throttle_settings" {
    for_each = var.throttle_settings
    content {
      burst_limit = throttle_settings.value.burst_limit
      rate_limit = throttle_settings.value.rate_limit
    }
  }

}
