variable "usage_plan_name" {}

variable "usage_plan_description" {}

variable "usage_plan_product_code" {}

variable "api_stages" {}

variable "quota_settings" {}

variable "throttle_settings" {}